import React, { useState } from "react";
import { Button,Card, Form, Row, Col, Container, Navbar, NavDropdown, Nav, Modal} from "react-bootstrap";
import { confirm } from "react-confirm-box";

import 'bootstrap/dist/css/bootstrap.min.css';
import ReactS3 from 'react-s3';
import ReactDom  from 'react-dom';
import { Link, Navigate, Redirect} from 'react-router-dom';

import axios from 'axios'; 
import { Trash, CloudUploadFill, PlusCircleFill} from 'react-bootstrap-icons';
import {ReactSession} from 'react-client-session';
var url_link = '';
    
  
class BucketList extends React.Component {

    fileObj = [];
    fileArray = [];
    constructor(props) {
        super(props)
        this.state = {
            bucket: [],
            images: [],
            show_modal : false,
            modal_title: '',
            modal_description: '',
            bucket_value : '',
            display_loader: ''
        }
        this.ondeleteBucket = this.ondeleteBucket.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

    }

    handleClose(e) {
        this.setState({show_modal:false});
    }
    handleDelete(e) {
        this.setState({modal_title: 'Delete'});
        this.setState({modal_description:'Are you sure you want to delete bucket '+e.target.value+ '?'});
        this.setState({bucket_value : e.target.value})
        this.setState({show_modal:true});
    }

    onCreateBucket(e) {
        var bucket_name = prompt('Please add Bucket name');
        if(bucket_name) {

            var token = 'Bearer '+ReactSession.get("access_token");
            var post_data = {  
                'bucket_name': bucket_name,
            };
            axios.post(`http://127.0.0.1:8000/api/createbucket`, 
            post_data,
            { headers: {
                authorization: token
                }
            })
            .then(res => {
                if(res.data.status) {
                    alert(res.data.message);
                    this.fetchBucket();
                }
        });
        }        
    }

    ondeleteBucket(event) {
        this.setState({show_modal:false});

        var token = 'Bearer '+ReactSession.get("access_token");
        var post_data = {  
            'bucket_name': event.target.value,
        };
        axios.post(`http://127.0.0.1:8000/api/deletebucket`, 
        post_data,
        { headers: {
            authorization: token
            }
        })
        .then(res => {
            if(res.data.status) {
                alert('Bucket deleted');
                this.fetchBucket();
            }
        });
    }
    fetchBucket() {
        var token = 'Bearer '+ReactSession.get("access_token");

        axios.post(`http://127.0.0.1:8000/api/lists3bucket`, [],{ headers: {
            authorization: token
            }
        } )
        .then(res => {
            console.log(res.data.message);
                this.setState({bucket:res.data.data});
        })
        this.state.display_loader = 'display-none';

    }


    componentDidMount() {
        this.fetchBucket();
    }

    render() {
         
    return (
            <Row>
                <h3>Bucket List</h3>
                <div>
                <Button variant = "success" className = 'createBucket' onClick={this.onCreateBucket.bind(this)} ><PlusCircleFill></PlusCircleFill> Create Bucket</Button>
                </div>

                <Modal show={this.state.show_modal} onHide={this.handleClose.bind(this)}>
                    <Modal.Header closeButton>
                    <Modal.Title>{this.state.modal_title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.state.modal_description}</Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose.bind(this)}>
                        Cancel
                    </Button>
                    <Button variant="primary" value = {this.state.bucket_value} onClick={this.ondeleteBucket.bind(this)}>
                        Delete
                    </Button>
                    </Modal.Footer>
                </Modal>

                <Row className="{` ${this.state.display_loader}`}">
                <Col xs = {12}>
                <div className={`loader-check ${this.state.display_loader}`} >

                    <div className ="spinner-grow text-primary ml20" role="status">
                    </div>
                    <div className ="spinner-grow text-primary" role="status">
                    </div>
                    <div className ="spinner-grow text-primary" role="status">
                    </div>
                    <div className ="spinner-grow text-primary" role="status">
                    </div>
                    <div className ="spinner-grow text-primary" role="status">
                    </div>
                </div> 
                </Col>
                </Row>  

            {this.state.bucket.map((bucketList, i) =>
                <Col xs = {2} className = "fileContainer">
                    <Button className = "deleteBucket" variant = "danger" onClick={this.handleDelete.bind(this)} value = {bucketList} ><Trash></Trash></Button>
                    <Link to = {`/fileList/${bucketList}`}><img className = "folderList" src = "/image/folder-icon.png"></img></Link>{bucketList.charAt(0).toUpperCase() + bucketList.slice(1)}</Col>
            )}
            </Row>
        );
    }
}

export default BucketList