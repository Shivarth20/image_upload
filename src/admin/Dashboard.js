import React, { useState } from "react";
import { Button,Card, Form, Row,Navbar, NavDropdown, Nav, NavLink, Col, Container  } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import { Link} from 'react-router-dom'
import * as Icon from 'react-bootstrap-icons';


import Home from "./HomeDisplay";
import Image from "./ImageDisplay";
import BucketList from "./BucketList";
import Users from "./UsersList";

import {ReactSession} from 'react-client-session';
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';


class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state ={
            page: ''
        }
        this.goToPage = this.goToPage.bind(this);
        this.destorySession = this.destorySession.bind(this);
    }
    goToPage(e) {
        this.setState({page: e.target.getAttribute("data")});
    }

    destorySession(e) {
        ReactSession.set("access_token", ''); 
    }
  
    render() {
        var location = window.location.href;
        this.state.page = location.split(/[/]+/).pop();

        let display_page = '';
        var page = this.state.page;
        if(page == 'dashboard') {
            display_page = <Home></Home>;
        } else if(page == 'files') {
            display_page = <BucketList></BucketList>;
        } else if(page == 'users') {
            display_page = <Users></Users>;
        }
    return (
         <Container>
                <Row>
                    <Col xs={2} className = "dashboard_sidebar col-20 sidebar">
                    <img className="dashboardLogo" src="https://minimal-kit-react.vercel.app/static/logo.svg" />
                     <div className = 'header-name'><img  src = 'https://i0.wp.com/www.cssscript.com/wp-content/uploads/2020/12/Customizable-SVG-Avatar-Generator-In-JavaScript-Avataaars.js.png'></img> {ReactSession.get("name")}</div>   
                     <div className = 'dashboard_menu sticky-top'>
                        <Link to = '/dashboard'><div onClick={this.goToPage.bind(this)} className={page == "dashboard" ? "active" : ""} data = 'dashboard'><Icon.HouseDoorFill className="mb6"></Icon.HouseDoorFill> <span>Home</span></div></Link>
                        <Link to = '/dashboard/users'><div onClick={this.goToPage.bind(this)} className={page == "users" ? "active" : ""} data = 'users'><Icon.PeopleFill  className="mb6"></Icon.PeopleFill> <span>Users</span></div></Link>
                        <Link to = '/dashboard/files'><div onClick={this.goToPage.bind(this)} className={page == "files" ? "active" : ""} data = 'files'><Icon.CollectionFill  className="mb6"></Icon.CollectionFill> <span>Bucket</span></div></Link>
                        <Link to = '/logout'><div onClick={this.destorySession.bind(this)}><Icon.Power  className="mb6"></Icon.Power > <span>Logout</span></div></Link>
                    </div>
                    </Col>
                    
                    <Col xs = {10} className="col-80">
                        <Row>
                        <Col xs = {10}></Col>
                        <Col xs = {2}>
                            <Nav className = 'float-right'>
                                <NavDropdown className = 'profile-popup' title = {<div><img width= "40px" src = 'https://i0.wp.com/www.cssscript.com/wp-content/uploads/2020/12/Customizable-SVG-Avatar-Generator-In-JavaScript-Avataaars.js.png'></img></div>}>
                                    <NavDropdown.Item><span className = "bold">{ReactSession.get("name")}</span></NavDropdown.Item>
                                    <NavDropdown.Item className = 'details'><span className = "bold">{ReactSession.get("email")}</span></NavDropdown.Item>
                                    <NavDropdown.Item>My Profile</NavDropdown.Item>
                                    <NavDropdown.Item>Setting</NavDropdown.Item>
                                    <NavDropdown.Item><Link to = "/logout"><Button variant = "secondary">Logout</Button></Link></NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                        </Col>
                    </Row>
        
                    <div className = "col-md-12 pt50 loading">
                    {display_page}
                    </div> 
                    </Col>
                </Row>
            </Container>
        );
    }
}


export default Dashboard