import React, { useState } from "react";
import { Button,Card, Form, Row, Col, Container, Breadcrumb, Modal  } from "react-bootstrap";
import {ReactSession} from 'react-client-session';
import { Link, Navigate} from 'react-router-dom';
import { Trash, CloudUploadFill, PlusCircleFill} from 'react-bootstrap-icons';

import 'bootstrap/dist/css/bootstrap.min.css';
import Popup from 'react-popup';
import axios from 'axios'; 
      
class ImageDisplay extends React.Component {

    fileObj = [];
    fileArray = [];
    constructor(props) {
    var location = window.location.href;
    var output = location.split(/[/]+/).pop();

        super(props)
        this.state = {
            bucket_image: [],
            bucket_folder: [],
            images: [],
            show_modal : false,
            bucket_name: output,
            modal_title: '',
            modal_description: '',
            img_value : '',
            display_loader : '',
            folder_breadcrumb: ''
        }
      
        this.ondeleteFile = this.ondeleteFile.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleFileUpload = this.handleFileUpload.bind(this);
 
    }
    handleClose(e) {
        this.setState({show_modal:false});
    }

    handleDelete(e) {
        this.setState({modal_title: 'Delete'});
        this.setState({modal_description:'Are you sure you want to delete image '+e.target.getAttribute("data")+ '?'});
        this.setState({img_value : e.target.getAttribute("data")})
        this.setState({show_modal:true});
    }

    ondeleteFile(event) {
        this.setState({show_modal:false});
        var token = 'Bearer '+ReactSession.get("access_token");
        var post_data = {  
            'bucket_name': this.state.bucket_name,
            'file' : event.target.value
        };
        axios.post(`http://127.0.0.1:8000/api/filedelete`, 
        post_data,
        { headers: {
            authorization: token
            }
        })
        .then(res => {
            if(res.data.status) {
                this.openDirectory();
            }
        });
    }

    handleFileUpload(e) {
    const data = new FormData();
    data.append('file', e.target.files[0]);
    data.append('bucket_name', this.state.bucket_name);
    data.append('path', 'profile');
    
    var token = 'Bearer '+ReactSession.get("access_token");

    axios.post(`http://127.0.0.1:8000/api/fileupload`, data, { headers: {
        authorization: token
        }
    })
    .then(res => {
            if(res.data.status) {
                alert(res.data.message);
                this.openDirectory();
            }
    })
    }
    
    onCreateDirectory(e) {
        Popup.alert('I am alert, nice to meet you');
        
        var directory_name = prompt('Please add directory name');
        if(directory_name) {

            var token = 'Bearer '+ReactSession.get("access_token");
            var post_data = {  
                'bucket_name': this.state.bucket_name,
                'directory_name' : directory_name
            };
            axios.post(`http://127.0.0.1:8000/api/createdirectory`, 
            post_data,
            { headers: {
                authorization: token
                }
            })
            .then(res => {
                if(res.data.status) {
                    alert(res.data.message);
                    this.fetchImage();
                }
        });
        }        
    }
    openDirectory(e) {
        
        this.state.display_loader = '';

        var token = 'Bearer '+ReactSession.get("access_token");
        var result = {};
        result['bucket_name'] = this.state.bucket_name; 
        result['directory_name'] = e.target.getAttribute("data");
        axios.post(`http://127.0.0.1:8000/api/filelist`, 
        result,
        { 
            headers: {
                authorization: token
            }
        })
        .then(res => {
            this.setState({bucket_image:res.data.data.file});
            this.setState({bucket_folder:{}});
        })
        this.state.display_loader = 'display-none';
        this.state.folder_breadcrumb = e.target.getAttribute("data");
    }

    fetchImage() {
        this.state.display_loader = '';

        var token = 'Bearer '+ReactSession.get("access_token");
        var result = {};
        result['bucket_name'] = this.state.bucket_name; 
        axios.post(`http://127.0.0.1:8000/api/filelist`, 
        result,
        { headers: {
            authorization: token
            }
        })
        .then(res => {
            this.setState({bucket_image:res.data.data.file});
            this.setState({bucket_folder:res.data.data.folder});
            console.log(this.state.bucket_image.length);
        })
        this.state.display_loader = 'display-none';

    }


    componentDidMount() {
        this.fetchImage();
    }

    render() {

        var bucket_list = '';
        var image_list = '';

        if (this.state.bucket_folder.length > 0) {

            bucket_list = this.state.bucket_folder.map((bucketList, i) =>
                <Col key = {i} xs = {2} className = "fileContainer text-center">
                    <Button className = "deleteBucket" variant = "danger" value = 'Delete' onClick={this.handleDelete.bind(this)} data = {bucketList} ><Trash></Trash></Button>
                    <img data = {bucketList['name']} onClick={this.openDirectory.bind(this)} className = "pointer bucketImage" src = "/image/folder-icon.png"></img><span>{bucketList['name'].charAt(0).toUpperCase() + bucketList['name'].slice(1)}</span></Col>
            );
        }
        
        if (this.state.bucket_image.length != 'undefined') {
            image_list =this.state.bucket_image.map((imgList, i) =>
                <Col xs = {3} className = "fileContainer">
                    <Button className = "deleteFile" variant = "danger" value = 'Delete' onClick={this.handleDelete.bind(this)} data = {imgList['name']} ><Trash></Trash></Button>
                    <img className = "pointer folderList" src = {imgList['url']}></img><span>{imgList['name'].charAt(0).toUpperCase() + imgList['name'].slice(1)}</span><span className = 'right-align'> {imgList['size']}</span></Col>
            )
        }

        var folder_breadcrumb = '';

        if(this.state.folder_breadcrumb) {
            folder_breadcrumb = <Breadcrumb.Item>{this.state.folder_breadcrumb}</Breadcrumb.Item>;
        }
    return (        
            <Row>
                <Col xs={8}>
                    <Breadcrumb>
                        <Breadcrumb.Item ><Link to =  '/dashboard/files'> Bucket List</Link></Breadcrumb.Item>
                        <Breadcrumb.Item >{this.state.bucket_name}</Breadcrumb.Item>
                        {folder_breadcrumb}
                    </Breadcrumb>
                </Col>

                <Col xs={4} className = "image-button">
                    <div className="float-right">
                    <Button variant = "success" onClick={this.onCreateDirectory.bind(this)}><PlusCircleFill></PlusCircleFill> Create Directory</Button>
                    <Button variant = "success" className = 'ml10' onClick={() => this.refs.fileInput.click()}><CloudUploadFill></CloudUploadFill> Upload</Button>
                    <input ref="fileInput" onChange={this.handleFileUpload} type="file" style={{ display: "none" }}/>
                    </div>
                </Col>
                <Modal show={this.state.show_modal} onHide={this.handleClose.bind(this)}>
                    <Modal.Header closeButton>
                    <Modal.Title>{this.state.modal_title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.state.modal_description}</Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose.bind(this)}>
                        Cancel
                    </Button>
                    <Button variant="primary" value = {this.state.img_value} onClick={this.ondeleteFile.bind(this)}>
                        Delete
                    </Button>
                    </Modal.Footer>
                </Modal>
             <div className="mt50"></div>
             <Row className="{` ${this.state.display_loader}`}">
                <Col xs = {12}>
                <div className={`loader-check ${this.state.display_loader}`} >

                    <div className ="spinner-grow text-primary ml20" role="status">
                    </div>
                    <div className ="spinner-grow text-primary" role="status">
                    </div>
                    <div className ="spinner-grow text-primary" role="status">
                    </div>
                    <div className ="spinner-grow text-primary" role="status">
                    </div>
                    <div className ="spinner-grow text-primary" role="status">
                    </div>
                </div> 
                </Col>
            </Row>
             {bucket_list}
             {image_list}   
              
            </Row>
        );
    }
}

export default ImageDisplay