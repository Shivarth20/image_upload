import React, { useState } from "react";
import { Button,Card, Form, Row, Col, Container, Breadcrumb, Modal  } from "react-bootstrap";
import {ReactSession} from 'react-client-session';
import { Link, Navigate} from 'react-router-dom';
import { CloudArrowUpFill, PeopleFill} from 'react-bootstrap-icons';

import 'bootstrap/dist/css/bootstrap.min.css';
import ReactS3 from 'react-s3';
import ReactDom  from 'react-dom';
import axios from 'axios'; 

    
  
class HomeDisplay extends React.Component {

    componentDidMount() {
    }

    render() {
    return (        
            <Row>
                <Col xs={3} className = 'dashboardInfo'>
                    <div className = "dashboardIcon bucketIcon">
                        <CloudArrowUpFill></CloudArrowUpFill>
                    </div>
                    <div className="text-center"><span  className="bucketIcon"><h2>6</h2> Buckets</span></div>
                </Col>
                <Col xs={3} className = 'dashboardInfo people'  >
                    <div className = "dashboardIcon peopleIcon">
                        <PeopleFill></PeopleFill>
                    </div>
                    <div className="text-center"><span className = 'peopleIcon'><h2>10</h2> Users</span></div>
                </Col>
            </Row>
        );
    }
}

export default HomeDisplay