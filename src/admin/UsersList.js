import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import MaterialTable from 'material-table'
import AddBox from "@material-ui/icons/AddBox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import tableIcons from "./MaterialTableIcons";
import {ReactSession} from 'react-client-session';
import axios from 'axios'; 

class UsersList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user_list: [],
        }
       
    }
    fetchUserList() {
        var token = 'Bearer '+ReactSession.get("access_token");
        var result = {};
        result['bucket_name'] = this.state.bucket_name; 
        axios.post(`http://127.0.0.1:8000/api/users_list`, 
        result,
        { headers: {
            authorization: token
            }
        })
        .then(res => {
            console.log(res);
            this.setState({user_list:res.data.data});
            console.log(this.state.user_list);
        })
    }
    componentDidMount() {
        this.fetchUserList();
    }
  render() {

      const columns = [
        { title: "User Id", field: "id" },
        { title: "Name", field: "name" },
        { title: "Email", field: "email"},
        { title: "User Type", field: "user_type",render: (rowData) => rowData.user_type ? 'Admin User': 'Normal User'},
        {
            title: "Action",
            render: (rowData) => {
              const button = (
                <tableIcons.Delete
                  onClick={() => {
                    alert("You want to delete " + rowData.name)
                }}
                >
                </tableIcons.Delete>
              );
              return button;
            }
          }
      ];      
      
    return (
      <div style={{ maxWidth: '100%' }}>
        <MaterialTable title="Table" icons={tableIcons} columns={columns} data={this.state.user_list}  title="Users List"       options={{
        paging:true,
        pageSize:10,       // make initial page size
        emptyRowsWhenPaging: false,   // To avoid of having empty rows
        pageSizeOptions:[10,20,30,50],    // rows selection options
      }}   actions={[
        {
          icon: tableIcons.Add,
          tooltip: "Add User",
          isFreeAction: true,
          onClick: (event) => alert("You want to add a new row"),
        },
      ]}
 />;
      </div>
    )
  }
}

export default UsersList