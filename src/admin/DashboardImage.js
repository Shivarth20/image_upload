import React, { useState } from "react";
import { Button,Card, Form, Row,Navbar, NavDropdown, Nav, Col, Container  } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';

import Aws from "./AwsDisplay";
import BucketList from "./BucketList";
import Image from "./ImageDisplay";
import { Link} from 'react-router-dom'
import * as Icon from 'react-bootstrap-icons';


import {ReactSession} from 'react-client-session';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'


class DashboardImage extends React.Component {

    constructor(props) {
        super(props);
        this.state ={
            page: 'BucketList'
        }
        this.goToPage = this.goToPage.bind(this);
        this.destorySession = this.destorySession.bind(this);

    }

    destorySession(e) {
        ReactSession.set("access_token", ''); 
    }

    goToPage(e) {
        this.setState({page: e.target.getAttribute("data")});
    }
 
    render() {
        
    return (
         <Container>
            <Row>
                <Col xs={2} className = "dashboard_sidebar col-20">
                    <img className="dashboardLogo" src="https://minimal-kit-react.vercel.app/static/logo.svg" />
                    <div className="sticky-top">
                     <div className = 'header-name'><img  src = 'https://i0.wp.com/www.cssscript.com/wp-content/uploads/2020/12/Customizable-SVG-Avatar-Generator-In-JavaScript-Avataaars.js.png'></img> {ReactSession.get("name")}</div>   
                         <div className = 'dashboard_menu'>
                        <Link to = '/dashboard'><div onClick={this.goToPage.bind(this)} data = 'dashboard'><Icon.HouseDoorFill className="mb6"></Icon.HouseDoorFill> Home</div></Link>
                        <Link to = '/dashboard/users'><div onClick={this.goToPage.bind(this)} data = 'users'><Icon.PeopleFill className="mb6"></Icon.PeopleFill> Users</div></Link>
                        <Link to = '/dashboard/files'><div onClick={this.goToPage.bind(this)} data = 'files' className="active"><Icon.CollectionFill className="mb6"></Icon.CollectionFill> Bucket</div></Link>
                        <Link to = '/logout'><div onClick={this.destorySession.bind(this)}><Icon.Power  className="mb6"></Icon.Power > <span>Logout</span></div></Link>
                    </div>
                    </div>
                    </Col>
                    <Col xs = {10} className="col-80">
                        <Row>
                        <Col xs = {10}></Col>
                        <Col xs = {2}>
                            <Nav className = "float-right">
                                <NavDropdown className = 'profile-popup' title = {<div><img width= "40px" src = 'https://i0.wp.com/www.cssscript.com/wp-content/uploads/2020/12/Customizable-SVG-Avatar-Generator-In-JavaScript-Avataaars.js.png'></img></div>}>
                                    <NavDropdown.Item><span className = "bold">{ReactSession.get("name")}</span></NavDropdown.Item>
                                    <NavDropdown.Item className = 'details'><span className = "bold">{ReactSession.get("email")}</span></NavDropdown.Item>
                                    <NavDropdown.Item>My Profile</NavDropdown.Item>
                                    <NavDropdown.Item>Setting</NavDropdown.Item>
                                    <NavDropdown.Item><Link to = "/logout"><Button variant = "secondary">Logout</Button></Link></NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                        </Col>
                    </Row>
        
                    <div className = "col-md-12 pt50 dashboard-content loading">
                        <Image></Image>

                    </div> 
                    </Col>
                </Row>
            </Container>
        );
    }
}


export default DashboardImage