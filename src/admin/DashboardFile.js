import React, { useState } from "react";
import { Button,Card, Form, Row,Navbar, NavDropdown, Nav, Col, Container  } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import Menu from "./Menu";
import { Link} from 'react-router-dom'

import BucketList from "./BucketList";

import {ReactSession} from 'react-client-session';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

class DashboardFile extends React.Component {

    constructor(props) {
        super(props);
        this.state ={
            page: 'BucketList'
        }
    }
  
    render() {
        
    return (
         <Container>
             <Row>
                <Col xs={2} className = "dashboard_sidebar col-20">
                    <img className="dashboardLogo" src="https://minimal-kit-react.vercel.app/static/logo.svg" />
                     <div className = 'header-name'><img  src = 'https://i0.wp.com/www.cssscript.com/wp-content/uploads/2020/12/Customizable-SVG-Avatar-Generator-In-JavaScript-Avataaars.js.png'></img> {ReactSession.get("name")}</div>   
                    <Menu></Menu>

                    </Col>
                    <Col xs = {10} className="col-80">
                        <Row>
                        <Col xs = {9}></Col>
                        <Col xs = {3}>
                            <Nav>
                                <NavDropdown className = 'float-right profile-popup' title = {<div><img width= "40px" src = 'https://i0.wp.com/www.cssscript.com/wp-content/uploads/2020/12/Customizable-SVG-Avatar-Generator-In-JavaScript-Avataaars.js.png'></img><span className = "font-cursive">{ReactSession.get("name")}</span></div>}>
                                    <NavDropdown.Item>My Profile</NavDropdown.Item>
                                    <NavDropdown.Item>Setting</NavDropdown.Item>
                                    <Link to = "/logout"><NavDropdown.Item><Button variant = "secondary">Logout</Button></NavDropdown.Item></Link>
                                </NavDropdown>
                            </Nav>
                        </Col>
                    </Row>
        
                    <div className = "col-md-12 pt50 dashboard-content loading">
                    <BucketList></BucketList>

                    </div> 
                    </Col>
                </Row>
            </Container>
        );
    }
}


export default DashboardFile