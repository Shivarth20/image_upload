import React, { useState } from "react";
import { Button,Card, Form, Row, Col, Container  } from "react-bootstrap";
import * as Icon from 'react-bootstrap-icons';


import 'bootstrap/dist/css/bootstrap.min.css';
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
//import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { Link} from 'react-router-dom'
  

class Menu extends React.Component {
  
    render() {

    return (
            <div className = 'dashboard_menu'>
                <Link to = '/dashboard'><div><Icon.HouseDoorFill></Icon.HouseDoorFill> Home</div></Link>
                <Link to = '/dashboard/users'><div><Icon.PeopleFill></Icon.PeopleFill> Users</div></Link>
                <Link to = '/dashboard/files'><div><Icon.CollectionFill></Icon.CollectionFill> Bucket</div></Link>
                <Link to = '/logout'><div><Icon.Playstation></Icon.Playstation> Logout</div></Link>
            </div>
        );
    }
}

export default Menu