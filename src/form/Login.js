import React, { useState } from "react";
import { Button,Card, Form, Row, Col, Container, Alert  } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link, Navigate, Redirect, useNavigate, useLocation} from 'react-router-dom';
import { ArrowLeftCircleFill} from 'react-bootstrap-icons';
import emailjs from '@emailjs/browser';


import axios from 'axios';
import {ReactSession} from 'react-client-session';


class Login extends React.Component {
    
    constructor(props) {
        super(props);
        this.state ={
            email: '',
            password: '',
            errors: {},
            display_login : 1,
            dispaly_mail:0,
            display_confirmation:1,
            authenticate: 0,
            otp: '',
            updateUserPassword: '',
            updateConfirmPassword: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onOTPChange = this.onOTPChange.bind(this);

    }

    handleChange(event) {

        this.setState({value: event.target.value.email});  
    }
    
    onOTPChange(e) {
        this.setState({otp: e.target.value.email});  
    }

    onUpdatePasswordChange(e) {
        console.log(e.target.value);
        this.setState({updateUserPassword: e.target.value.updateUserPassword});  
    }    
    
    onupdateConfirmPasswordChange(e) {
        this.setState({updateConfirmPassword: e.target.value.email});  
    }

    onEmailChange(e) {
        this.setState({email: e.target.value});
    }

    onForgotPasswordRequest(e) {
        this.setState({display_login: 2});
    }

    handleOtpRequest(e) {
        axios.post(`http://127.0.0.1:8000/api/generateotp`, {
            "email":this.state.email
        })
            .then(res => {
                if(res.data.status) {
                    this.setState({display_login: 3});
                } else {
                    let error_list = {};
                    error_list["otp"] = res.data.message;
                    this.setState({errors: error_list});
                }
            })
    }

    verifyOtpRequest(e) {
        this.setState({display_login: 4});
    }

    updatePassword(e) {
        this.setState({display_login: 5});
    }

    gotoLogin(e) {
        this.setState({display_login: 1});
    }

    goBack(e) {
        this.setState({display_login: this.state.display_login - 1});   
    }
      
    onPasswordChange(e) {
        this.setState({password: e.target.value});
        //this.clearValidationErr("password");
    }

    ChangeState(e) {
        //const [login_type, setPage] = useState("Register");
    } 
    
    validateForm() {

        let error_list = {};
        let formIsValid = true;
  
        if (!this.state.email) {
          formIsValid = false;
          error_list["email"] = "*Please enter your email-ID.";
        }
  
        if (typeof this.state.email !== "undefined") {
          //regular expression for email validation
          var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
          if (!pattern.test(this.state.email)) {
            formIsValid = false;
            error_list["email"] = "*Please enter valid email-ID.";
          }
        }
    
        if (!this.state.password) {
          formIsValid = false;
          error_list["password"] = "*Please enter your password.";
        }

        this.setState({errors: error_list});

        return formIsValid;  
  
      }
  
  
    handleSubmit(event) {

        event.preventDefault();

        if (this.validateForm()) {
            //document.body.classList.add('loader');

            axios.post(`http://127.0.0.1:8000/api/login`, {
                "email":this.state.email,
                "password": this.state.password
            })
                .then(res => {
                    if(res.data.status) {
                        ReactSession.set("access_token", res.data.access_token); 
                        ReactSession.set("email", res.data.user_details.email); 
                        ReactSession.set("name", res.data.user_details.name);
                        this.setState({authenticate: 1});
                    } else {
                        let error_list = {};
                        error_list["password"] = res.data.message;
                        this.setState({errors: error_list});
                    }
                })
        } 

    }
    componentDidMount() {
        }
    render() {

        if(this.state.authenticate || ReactSession.get("access_token")) {
            return <Navigate to='/dashboard'/>;
        }

        const display = this.state.display_login;
        let form;

        if (display == 1) {
            form =  <div className = "login_section">
            <h3>Sign in to Minimal</h3>
            <br></br> <span className="fw10">Enter your details below.</span>
                <Form className = "login_form">
                    <Form.Group>
                        <Form.Label  className = "mt-2">Email</Form.Label>
                        <Form.Control type = "email" value = {this.state.email} onChange={this.onEmailChange.bind(this)} placeholder="Enter email id"></Form.Control>
                        <div className="errorMsg">{this.state.errors.email}</div>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label className = "mt-2">Password</Form.Label>
                        <Form.Control type = "password" value = {this.state.password} onChange={this.onPasswordChange.bind(this)} placeholder="Enter Password"></Form.Control>
                        <div className="errorMsg">{this.state.errors.password}</div>
                    </Form.Group>
                    <span className="float-right mt10 pointer" onClick = {this.onForgotPasswordRequest.bind(this)}>Forgot password?</span>
                    <Form.Group>
                        <Button className="mt-3 w100" variant = "success" disabled={!this.validateForm} onClick={this.handleSubmit} >Login</Button>
                    </Form.Group>
                </Form> 
        </div>;
        } else if(display == 2) {
            form = <div className = "request-otp">
            <ArrowLeftCircleFill className = 'go-back' onClick={this.goBack.bind(this)}></ArrowLeftCircleFill><br /><br />
            <h3>Password Update</h3>
            <span className="fw10">Enter your details below.</span><br/><br/>
            <Form className = "login_form">
                <Form.Group>
                    <Form.Label  className = "mt-2">Email</Form.Label>
                    <Form.Control type = "email" value = {this.state.email} onChange={this.onEmailChange.bind(this)} placeholder="Enter email id"></Form.Control>
                    <div className="errorMsg">{this.state.errors.email}</div>
                </Form.Group>
                <Form.Group>
                    <Button className="mt-3 w100" variant = "success" onClick={this.handleOtpRequest.bind(this)} >Send OTP</Button>
                </Form.Group>
            </Form>
            </div>;
        } else if(display == 3) {
            form = <div className = "request-otp">
            <ArrowLeftCircleFill className = 'go-back' onClick={this.goBack.bind(this)}></ArrowLeftCircleFill><br /><br />    
            <h3>Verify OTP</h3>
            <span className="fw10">Please check your mail and enter OTP.</span><br/><br/>
            <Form className = "login_form">
                <Form.Group>
                    <Form.Label  className = "mt-2">OTP</Form.Label>
                    <Form.Control type = "text" value = {this.state.otp} onChange={this.onOTPChange.bind(this)} placeholder="Enter OTP"></Form.Control>
                    <div className="errorMsg">{this.state.errors.otp}</div>
                </Form.Group>
                <Form.Group>
                    <Button className="mt-3 w100" variant = "success" onClick={this.verifyOtpRequest.bind(this)} >Verify OTP</Button>
                </Form.Group>
            </Form>
            </div>;
        } else if(display == 4) {
            form = <div className = "password-confirmation">
                        <ArrowLeftCircleFill className = 'go-back' onClick={this.goBack.bind(this)}></ArrowLeftCircleFill><br /><br />
                        <h3>Update Password</h3>
                        <span className="fw10">Update password here.</span><br/><br/>
                            <Form className = "login_form">
                            <Form.Group>
                                    <Form.Label className = "mt-2">Password</Form.Label>
                                    <Form.Control type = "password" value = {this.state.updateUserPassword} onChange={this.onUpdatePasswordChange.bind(this)} placeholder="Enter Password"></Form.Control>
                                    <div className="errorMsg">{this.state.errors.password}</div>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label className = "mt-2"> Confirm Password</Form.Label>
                                <Form.Control type = "password" value = {this.state.updateConfirmPassword} onChange={this.onupdateConfirmPasswordChange.bind(this)} placeholder="Confirm Password"></Form.Control>
                                <div className="errorMsg">{this.state.errors.password}</div>
                            </Form.Group>
                                <Form.Group>
                                    <Button className="mt-3 w100" variant = "success" onClick={this.updatePassword.bind(this)} >Update</Button>
                                </Form.Group>
                            </Form>
                    </div> ;
        }else if(display == 5) {
            form = <div className = "password-confirmation">
                        <ArrowLeftCircleFill className = 'go-back' onClick={this.goBack.bind(this)}></ArrowLeftCircleFill><br /><br />
                        <h3>Password Updated Succesfully</h3>
                        <span onClick={this.gotoLogin.bind(this)}>Click here</span> to login
                    </div> ;
        }

      //  return <Redirect to='/dashboard'/>

    return (
            <Container className = "headerForm">
                <Row>
                    <Col xs={5}>
                        <div className="col-md-11 registeration_image">
                            <img className="logo" src = "https://minimal-kit-react.vercel.app/static/logo.svg"></img>
                            <h3 className="login_header pt50">Hi, Welcome back</h3>
                            <img  src = "https://minimal-kit-react.vercel.app/static/illustrations/illustration_login.png"></img>
                        </div>    
                    </Col>
                    <Col xs = {7}>
        
                    <div className = "offset-md-1 col-md-11 pl40 pr20 pt20">
                        <span className = "float-right">Don’t have an account? <Link to = "/register" >Get started</Link></span>
                        <div className = "mt100">
                            {form}
                        </div>       
                    </div> 
                    </Col>
                </Row>
            </Container>
            );
    }
}


export default Login