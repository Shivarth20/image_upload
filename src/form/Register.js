import React, { useState } from "react";
import { Button,Card, Form, Row, Col, Container  } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import { Link} from 'react-router-dom'

//import { styled } from '@mui/material/styles';
//import { Box, Card, Link, Container, Typography } from '@mui/material';


class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            email: '',
            password: '',
            confirmpassword: '',
            name : '',
            errors: {}
        }
        this.handleSubmit = this.handleSubmit.bind(this);

    }
  
    validateForm() {

        let error_list = {};
        let formIsValid = true;
  
        if (!this.state.email) {
          formIsValid = false;
          error_list["email"] = "*Please enter your email-ID.";
        } else {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(this.state.email)) {
              formIsValid = false;
              error_list["email"] = "*Please enter valid email-ID.";
            }   
        }
    
        if (!this.state.password) {
          formIsValid = false;
          error_list["password"] = "*Please enter your password.";
        }

        if (!this.state.name) {
            formIsValid = false;
            error_list["name"] = "*Please enter your name.";
        }

        if (!this.state.confirmpassword) {
            formIsValid = false;
            error_list["confirmpassword"] = "*Please enter confirm password.";
        } else {
            if(this.state.confirmpassword !== this.state.password) {
                formIsValid = false;
                error_list["confirmpassword"] = "Password does not match";    
            }
        }

        this.setState({errors: error_list});
        console.log(error_list);

        return formIsValid;  
  
    }

    onEmailChange(e) {
        this.setState({email: e.target.value});
      }
      
    onPasswordChange(e) {
        this.setState({password: e.target.value});
    }

    onConfirmPasswordChange(e) {
        this.setState({confirmpassword: e.target.value});
    }
    
    onNameChange(e) {
        this.setState({name: e.target.value});
    }
  
    handleSubmit(event) {

        event.preventDefault();
        if (this.validateForm()) {
            axios.post(`http://127.0.0.1:8000/api/register`, {
                "email":this.state.email,
                "password":this.state.password,
                "name":this.state.name,
                "contactnos":this.state.contactnos,
                "password_confirmation":this.state.password
            })
            .then(res => {
                console.log(res.data);
                if(res.data.status) {
                    this.setState({name: ''});
                    this.setState({password: ''});
                    this.setState({email: ''});
                    this.setState({contactnos: ''});
                    this.setState({confirmpassword: ''});
                    alert('Suceesfully Register');
                } else {
                    console.log('sd');
                    this.setState({errors: res.data.error});
                }
            })
        }
    }
    render() {
    return (
        <Container className = "headerForm">
        <Row>
            <Col xs={5}>
                <div className="col-md-11 registeration_image">
                    <img className="logo" src = "https://minimal-kit-react.vercel.app/static/logo.svg"></img>
                    <h3 className="login_header pt50">Manage the job more effectively with Minimal</h3>
                    <img  src = "https://minimal-kit-react.vercel.app/static/illustrations/illustration_login.png"></img>
                </div>    
            </Col>
            <Col xs = {7}>
            <div className = "offset-md-1 col-md-11 pl40 pr20 pt20">
                <span className = "float-right">Already have an account? <Link to = "/login" >Login</Link></span>
                <h3 className="pt50">Get started absolutely free.                </h3>
                <span className="fw10">Free forever. No credit card needed.</span>
                <Form className = "registerationForm">
                    <Form.Group>
                        <Form.Label  className = "mt-2">Email</Form.Label>
                        <Form.Control className="form-rounded" type = "email" value = {this.state.email} onChange={this.onEmailChange.bind(this)} placeholder="Enter email id"></Form.Control>
                        <div className="errorMsg">{this.state.errors.email}</div>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label className = "mt-2">Name</Form.Label>
                        <Form.Control type = "text" value = {this.state.name} onChange={this.onNameChange.bind(this)} placeholder="Enter Name"></Form.Control>
                        <div className="errorMsg">{this.state.errors.name}</div>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label className = "mt-2">Password</Form.Label>
                        <Form.Control type = "password" value = {this.state.password} onChange={this.onPasswordChange.bind(this)} placeholder="Enter Password"></Form.Control>
                        <div className="errorMsg">{this.state.errors.password}</div>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label className = "mt-2">Confirm Password</Form.Label>
                        <Form.Control type = "password" value = {this.state.confirmpassword} onChange={this.onConfirmPasswordChange.bind(this)} placeholder="Enter Password"></Form.Control>
                        <div className="errorMsg">{this.state.errors.confirmpassword}</div>
                    </Form.Group>
                    <Form.Group>
                        <Button className="mt-5 w100" variant="success" onClick={this.handleSubmit} >Register</Button>
                    </Form.Group>
                </Form> 
            </div>
            </Col>
        </Row>
    </Container>
    );
    }
}

export default Register