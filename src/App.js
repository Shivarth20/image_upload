import react, { useState } from "react";
import Login from "./form/Login";
import Register from "./form/Register";
import './index.css';
import { BrowserRouter as Router, Route, Routes, Redirect } from 'react-router-dom'
import Dashboard from "./admin/Dashboard";
import Files from "./admin/DashboardImage";
import { Link, Navigate} from 'react-router-dom';

import {ReactSession} from 'react-client-session';


const App = () => {
  ReactSession.setStoreType("localStorage");
  //const navigate = useNavigate()

  return (

    <Router>
        <Routes>
          <Route exact path="/register" element={<Register/>}/>
          <Route exact path="/login" element={<Login/>}/>
          <Route exact path="/" element={<Login/>}/>
          <Route exact path="/dashboard/*" element={<Dashboard/>}/>
          <Route exact path="/filelist/*" element={<Files/>}/>
          <Route exact path = "/logout" element = {<Navigate to ="/login"/>}></Route>

        </Routes>
    </Router>
  );
}

export default App;